/* version 3.0.0 */

syntax = "proto3";

message DroneState {
  enum ControlState {
    DEFAULT = 0;
    INIT = 1;
    LANDED = 2;
    FLYING = 3;
    HOVERING = 4;
    TEST = 5;
    TRANS_TAKEOFF = 6;
    TRANS_GOTOFIX = 7;
    TRANS_LANDING = 8;
    TRANS_LOOPING = 9;
  }

  enum FlyState {
    OK = 0;
    LOST_ALT = 1;
    LOST_ALT_GO_DOWN = 2;
    OUT_ALT_ZONE = 3;
    COMBINED_YAW = 4;
    BRAKE = 5;
    NO_VISION = 6;
  }

  /* 1 to 10 */
  ControlState controlState = 1;
  FlyState flyState = 2;

  /* 11 to 60 */
  bool acquisitionThreadOn = 11;
  bool adcWatchdogDelay = 12;
  bool altitudeControlAlgorithm = 13;
  bool anglesOutOfRange = 14;
  bool atCodecThreadOn = 15;
  bool cameraReady = 16;
  bool comWatchdogProblem = 17;
  bool communicationLost = 18;
  bool controlAlgorithm = 19;
  bool controlCommandAck = 20;
  bool controlWatchdogDelay = 21;
  bool cutoutDetected = 22;
  bool emergencyLanding = 23;
  bool flying = 24;
  bool gpsEphemerisStatus = 25;
  bool gpsFirmwareStatus = 26;
  bool gpsPlugged = 27;
  bool lowBattery = 28;
  bool magnometerNeedsCalibration = 29;
  bool motorProblem = 30;
  bool navdataBootstrap = 31;
  bool navdataDemo = 32;
  bool navdataThreadOn = 33;
  bool picVersionNumberOk = 34;
  bool softwareFault = 35;
  bool startButtonState = 36;
  bool timerElapsed = 37;
  bool tooMuchWind = 38;
  bool travellingEnabled = 39;
  bool ultrasonicSensorDeaf = 40;
  bool usbReady = 41;
  bool userEmergencyLanding = 42;
  bool videoEnabled = 43;
  bool videoThreadOn = 44;
  bool visionEnabled = 45;
  bool wifiLinkQuality = 46;
}

message FlightLog {
  enum Type {
    DEFAULT = 0;
    COMMAND = 1;
  }

  required string message = 1;
  required int64 timestamp = 2;
  required Type type = 3;
}

message NavData {
  message GPSData {
    message Satellite {
      required int32 id = 1;
      required int32 signalStrength = 2;
    }

    float latitude = 1;
    float longitude = 2;
    float elevation = 3;
    repeated Satellite satellites = 4;
  }

  message PWMData {
    repeated int32 motors = 1;
  }

  message ThreeAxisData {
    float x = 1;
    float y = 2;
    float z = 3;
  }

  /* 1 to 10 */
  int32 battery = 1;
  required DroneState droneState = 2;
  required GPSData gps = 3;
  required PWMData pwm = 4;
  required int64 timestamp = 5;

  /* 11 to 20 */
  required ThreeAxisData acceleration = 11;
  required ThreeAxisData gyroscope = 12;
  required ThreeAxisData magnetization = 13;
  required ThreeAxisData velocity = 14;
  required ThreeAxisData rotation = 15;
}

message Flight {
  /* 1 to 10 */
  required string id = 1;
  required string machineId = 2;

  /* 11 to 20 */
  int64 startTimestamp = 11;
  int64 endTimestamp = 12;
}

message FlightData {
  repeated FlightLog logs = 1;
  repeated NavData navData = 2;
  required Flight flight = 3;
  int32 version = 4;
}
