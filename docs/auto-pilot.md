# AutoPilot

Before extending class with the `AutoPilot` class, check that GPS coordinates are not 0 or near to 0.


## Methods

### AutoPilot#goToCoordinates(lat, lng)

**Deprecated.** Goes to (lat, lng) by rotating and going front. Returns a promise.

### AutoPilot#goToCoordinates2(lat, lng)

Goes to (lat, lng) without looking at the target. Angles are calculated using the [Haversine formula](https://en.wikipedia.org/wiki/Haversine_formula). Returns a promise.

```js
pilot.goToCoordinates2(45.21650902, 5.8081249, {
  // these are default values

  maxSpeed: 0.2, // number between 0 and 1
  maxPositionMarginOfError: 2 // positive meters (radius around target)

  // to be added
  directionCheckDelay: 500, // ms
  positionCheckDelay: 200 // ms
});
```

### AutoPilot#rotateToAngle(angle)

`angle` must be an angle in degrees between -180 and 180. Returns a promise.

```js
pilot.rotateToAngle(45, {
  marginOfError: 2, // positive degrees
  maxSlowAngle = 45, // positive degrees
  maxSpeed = 1, // number between 0 and 1 (speed when difference angle is greater than 'maxSlowAngle')
  rotationCheckDelay = 100, // ms
  slowSpeed = 0.4 // number between 0 and 1 (speed when difference angle is lower than 'maxSlowAngle')
});
```

### AutoPilot#rotateToRelativeAngle(angle)

`angle` must be an angle in degrees between -180 and 180. Returns a promise.

See `AutoPilot#rotateToAngle()` for options.

### AutoPilot#runTrack(points)

Powered by `AutoPilot#goToCoordinates2()`.

```js
// elevation is not supported
pilot.runTrack([
  { latitude: 45.21650902, longitude: 5.8081249, elevation: 226 },
  { latitude: 45.21652969, longitude: 5.8076882, elevation: 227 }
])
```
