# Pilots


```js
const Pilot = require('drone');

class MyPilot extends Pilot {
  constructor() {
    super();
  }

  start() {
    return Promise.resolve()
      .then(() => this.controller.takeOff())
      .then(() => this.controller.moveUp(0.5, 3e3))
      .then(() => this.controller.animateLeds('blinkRed', 5, 2))
      .then(() => this.controller.takePhoto('foobar.png'))
      // keeps the drone flying until you press Ctrl+C
      .then(() => Promise.resolve().promise);
  }

  stop() {
    return Promise.resolve()
      // make sure the drone is not moving
      .then(() => this.controller.stop())
      .then(() => this.controller.land());
  }
}
```
