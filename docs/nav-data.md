# Navigation data


## GPS

```js
gps.latitude
gps.longitude
gps.elevation
```


## Sensors

### Acceleration

```js
acceleration.x
acceleration.y
acceleration.z
```

### Gyroscope


```js
// -180 to 180 (degrees)

gyroscope.x
gyroscope.y
gyroscope.z
```

### Rotation

```js
// -180 to 180 (degrees)

rotation.x
rotation.y
rotation.z
```

### Velocity

```js
// mm/s

velocity.x
velocity.y
velocity.z
```


## Others

### Battery

0 to 100

### Drone state

```js
// 'default', 'init', 'landed', 'flying', 'hovering', 'test', trans_takeoff', 'trans_gotofix', 'trans_landing' or 'trans_looping'
droneState.controlState

// 'ok', 'lost_alt', 'lost_alt_go_down', 'alt_out_zone', 'combined_yaw', 'brake' or 'no_vision'
droneState.flyState

// booleans
// properties marked with (*) will cancel take off if they are true
droneState.acquisitionThreadOn
droneState.adcWatchdogDelay
droneState.altitudeControlAlgorithm
(*) droneState.anglesOutOfRange
droneState.atCodecThreadOn
droneState.cameraReady
droneState.comWatchdogProblem
droneState.communicationLost
droneState.controlAlgorithm
droneState.controlCommandAck
droneState.controlWatchdogDelay
(*) droneState.cutoutDetected
droneState.emergencyLanding
droneState.flying
droneState.gpsEphemerisStatus
droneState.gpsFirmwareStatus
droneState.gpsPlugged
(*) droneState.lowBattery
(*) droneState.magnometerNeedsCalibration
(*) droneState.motorProblem
droneState.navdataBootstrap
droneState.navdataDemo
droneState.navdataThreadOn
droneState.picVersionNumberOk
(*) droneState.softwareFault
droneState.startButtonState
droneState.timerElapsed
(*) droneState.tooMuchWind
droneState.travellingEnabled
(*) droneState.ultrasonicSensorDeaf
droneState.usbReady
droneState.userEmergencyLanding
droneState.videoEnabled
droneState.videoThreadOn
droneState.visionEnabled
droneState.wifiLinkQuality
```

### PWM (pulse-width modulation)

```js
// array of four values between 0 and 255
droneState.pwm.motors
```
