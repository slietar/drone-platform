# Reporters


* `Reporter#begin()` : receives the flight object
* `Reporter#start()` : starts displaying or saving data
* (multiple) `Reporter#log()`
* (multiple) `Reporter#update()`
* `Reporter#stop()`
* `Reporter#end()`


```js
class MyReporter {
  // once the flight object is known (always first)
  begin(flight) {

  }

  // once the flight successfully ended
  end() {

  }

  log(log) {
    // log.flight
    // log.message
    // log.timestamp
    // log.type ('command' or 'custom')
  }

  // once the flight starts
  // you may now display whatever you want
  start() {

  }

  // once the flight is stopped
  stop() {

  }

  update(navData) {
    // navData.hasGps
    //
  }
}
```

Extending `DefaultReporter` will make all methods optional.
