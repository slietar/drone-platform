# Drone platform


## Installation

```bash
$ brew install node
$ brew install ffmpeg # to take photos

$ git clone https://gitlab.com/slietar/drone-platform.git
$ cd drone-platform
$ npm install --production
```


## Usage

```bash
# 'sudo' is required for WiFi manipulations
$ sudo npm start
```

### Default mode

```bash
# using the default control tower, you must add the '--unlocked' option
# at the end of each command for the drone to take off

# 'my-pilot' corresponds to 'lib/pilots/my-pilot.js'
$ npm start -- --pilot my-pilot

# 'dashboard' corresponds to 'lib/reporters/dashboard.js' or 'lib/reporters/dashboard/index.js'
$ npm start -- --reporters dashboard

# 'my-control-tower' corresponds to 'lib/control-towers/my-control-tower.js'
$ npm start -- --control-tower my-control-tower


# a 'drone network' is a WiFi network with an SSID matching '/ardrone2_v\d+.\d+.\d+/'

$ npm start -- --wifi ardrone2_v2.4.8  # connects to 'ardrone2_v2.4.8' network
$ npm start -- --wifi intitial         # stays on the initial network if no drone network is detected
$ npm start -- --wifi force-initial    # always stays on the initial network
$ npm start -- --wifi fake             # always stays on initial network without searching for
                                       # other networks (faster but signal strength will not be checked)
```

### Replay mode

```bash
# 52039 is the flight id given in default mode
$ npm run replay -- 52039
$ npm run replay -- 52039 --reporter presets/replay
```

Flight data sizes:

* JSON: ~ 7 MB / min
* protocol buffers (v1): ~ 700 kB / min (- 90% compared to JSON v1)
* protocol buffers (v3): ~ 1.3 MB / min (- 81 % compared to JSON v1; + 85% compared to protobuf v1)

### REPL mode

```bash
$ npm run repl
$ npm run repl -- --wifi (...)

$ npm run repl
> fly --pilot idle --unlocked
> fly --pilot idle-on-ground --reporter presets/map
> exit
```

### Manual mode

Go to [the `ar-drone` package's documentation](https://github.com/felixge/node-ar-drone) for more information. You must already be connected to the drone's WiFi network to use this mode.

```bash
$ npm run manual
drone> takeoff();
drone> calibrate(0);
drone> land();
```


## Available classes

### Control towers

* `ControlTower` (abstract): allows every take off and landing
* `DefaultControlTower` (default): forces `--unlocked` option and WiFi signal strength < -90 dBm (see this [‘Acceptable signal strengths’](https://support.metageek.com/hc/en-us/articles/201955754-Acceptable-WiFi-Signal-Strengths) on metageek.com)

### Pilots

* `Pilot` (abstract): ends flight immediatly
* `DefaultPilot` (default): animates leds before ending flight
* `AutoPilot`: follows a given GPX track
* `IdlePilot`: keeps flying (useful for testing reporters)
* `IdleOnGroundPilot`: never ends flight

### Reporters

* `Reporter` (abstract): reports nothing
* `AccelerationMapReporter`: creates a GeoJSON file according to acceleration navigation data
* `DashboardReporter`: shows a dashboard in which you can put widgets with real-time navigation data
* `FlightRecorderReporter`: saves all navigation data to a JSON file (needed to use replay mode later)
* `GPSMapReporter`: creates a GeoJSON file according to GPS navigation data
* `LogReporter`: displays logs
* `MainReporter`: combines multiple reporters
* `RealtimeMapReporter`: hosts an HTTP server with GeoJSON data for the realtime map clients

### Reporter presets

Reporter presets are available through the `presets/xxx` path.

* `DefaultReporterPreset`: combines `FlightRecorderReporter` and `LogReporter`
* `MapReporterPreset`: combines `FlightRecorderReporter` and `RealtimeMapReporter`
* `ReplayReporterPreset`: alias for `DashboardReporter`

### Dashboard widgets

* `AccelerationWidget` (line chart)
* `BatteryWidget` (gauge)
* `ElevationWidget` (line chart)
* `FlightLogWidget` (log)
* `GPSLogWidget` (log)
* `GyroscopeWidget` (line chart)
* `LatitudeWidget` (line chart)
* `LongitudeWidget` (line chart)
* `MotorsWidget` (bars)
* `RotationWidget` (line chart)
* `SatellitesWidget` (bars)
* `VelocityWidget` (line chart)


## Security

Using the default control tower, conditions to take off:

* `--unlocked` option must be on
* WiFi signal strength must be < -90 dBm
* `droneState.anglesOutOfRange` must be false
* `droneState.cutoutDetected` must be false
* `droneState.lowBattery` must be false
* `droneState.magnometerNeedsCalibration` must be false
* `droneState.motorProblem` must be false
* `droneState.softwareFault` must be false
* `droneState.tooMuchWind` must be false
* `droneState.ultrasonicSensorDeaf` must be false
* `droneState.userEmergencyLanding` must be false

Conditions to land:

* `--unlocked` option must be on


## Common errors

* `Error: ACK Timeout`: commands sent to the drone (often configuration commands) timed out


## Development

### Ports

* 3210: realtime map page
* 3211: realtime map data
* 3220: follower page
* 3221: follower sockets

### Issues

* flight recording/replay may not work because of a problem with sometimes missing fly state (commit dcd9c8a prevents potential crashes but makes nav data incomplete)
* realtime map doesn't work well in replay mode

### Critical/non-critical parts

> A critical function is a function which may throw an uncaught error, causing the program to exit without making the drone land. Their should be the least possible of those.

* `Drone#listenForNavData()`
* `DroneController#land()`
* `ControlTower#requestLanding()`

### Linting

```bash
$ npm run lint
```

### GPS protocol

1. `gps.firmwareStatus`: 0 -> 1
2. LEDs blinking
3. `gps.gpsPlugged`: 0 -> 1
4. `elevation`, `lastFrameTimestamp`, `c_n0`, `channels`: 0 -> x
