const path = require('path');

const fs = require('../mz/fs');
const xml2js = require('../mz/xml2js');


module.exports = function (filename) {
  let gpxPath = path.join(__dirname, '../../input/gpx', filename);

  return fs.readFile(gpxPath)
    .then((data) => xml2js.parseString(data))
    .then((data) => {
      let points = [];

      for (let trkpt of data.gpx.trk[0].trkseg[0].trkpt) {
        points.push({
          latitude: parseFloat(trkpt.$.lat),
          longitude: parseFloat(trkpt.$.lon),
          elevation: parseInt(trkpt.ele[0])
        });
      }

      return points;
    });
};
