const IntervalStatus = {
  running: 0,
  paused: 1,
  ended: 2
};

class Interval {
  constructor(handler, delay) {
    this.deferred = Promise.defer();
    this.intervalId = -1;
    this.options = { delay, handler };

    this.frame = -1;
    this.status = IntervalStatus.running;
    this._setup();
  }

  end(value) {
    if (this.status === IntervalStatus.ended) {
      return;
    }

    this.status = IntervalStatus.ended;
    this._stop();
    this.deferred.resolve(value);
  }

  pause() {
    if (this.status !== IntervalStatus.running) {
      return;
    }

    this.status = IntervalStatus.paused;
    this._stop();
  }

  get promise() {
    return this.deferred.promise;
  }

  pauseWithPromise(promise) {
    this.pause();

    return Promise.resolve(promise)
      .then(() => {
        this.resume();
      });
  }

  resume() {
    if (this.status !== IntervalStatus.paused) {
      return;
    }

    this.status = IntervalStatus.running;
    this._setup();
  }


  _handle() {
    // whaat ?
    if (this.status !== IntervalStatus.running) {
      return;
    }

    this.frame++;

    let result = Reflect.apply(this.options.handler, this, [this]);

    if (typeof result === 'object' && result instanceof Promise) {
      this.pauseWithPromise(result);
    }
  }

  _setup() {
    this._handle();

    this.intervalId = setInterval(() => this._handle(), this.options.delay);
  }

  _stop() {
    clearInterval(this.intervalId);
    this.intervalId = -1;
  }
}


module.exports = Interval;
