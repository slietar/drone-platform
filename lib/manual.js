const ardrone = require('ar-drone');
const mzArdrone = require('./mz/ar-drone');

let client = mzArdrone(ardrone.createClient());

Promise.all([
  client.config({ key: 'general:navdata_demo', value: 'FALSE' }),
  client.config({ key: 'general:navdata_options', value: 2147483647 })
])
  .then(() => {
    client.createRepl();
  })
  .catch((err) => {
    console.error(err);
  });
