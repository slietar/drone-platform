const FlightRecorderReporter = require('../flight-recorder');
const LogReporter = require('../log');
const MainReporter = require('../main');


class DefaultReporterPreset extends MainReporter {
  constructor() {
    super([
      new LogReporter(),
      new FlightRecorderReporter()
    ]);
  }
}


module.exports = DefaultReporterPreset;
