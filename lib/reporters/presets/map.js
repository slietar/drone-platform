const FlightRecorderReporter = require('../flight-recorder');
const LogReporter = require('../log');
const MainReporter = require('../main');
const RealtimeMapReporter = require('../realtime-map');


class MapReporterPreset extends MainReporter {
  constructor() {
    super([
      new LogReporter(),
      new RealtimeMapReporter(),
      new FlightRecorderReporter()
    ]);
  }
}


module.exports = MapReporterPreset;
