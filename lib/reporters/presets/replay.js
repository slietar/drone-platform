const DashboardReporter = require('../dashboard');
const MainReporter = require('../main');


class ReplayReporterPreset extends MainReporter {
  constructor() {
    super([
      new DashboardReporter()
    ]);
  }
}


module.exports = ReplayReporterPreset;
