module.exports = {
  start() {
    return {
      type: 'line',
      historyLength: 50,

      label: ' Velocity ',
      xLabelPadding: 3,
      xPadding: 5,
      showLegend: true,
      wholeNumbersOnly: true,
      style: { text: 'green', baseline: 'cyan' },

      lines: [
        { title: 'X',
          label: 'x',
          style: { line: 'red' } },
        { title: 'Y',
          label: 'y',
          style: { line: 'yellow' } },
        { title: 'Z',
          label: 'z',
          style: { line: 'green' } }
      ]
    };
  },
  processLines(navData) {
    return {
      x: navData.velocity.x + 1000,
      y: navData.velocity.y + 1000,
      z: navData.velocity.z + 1000
    };
  }
};
