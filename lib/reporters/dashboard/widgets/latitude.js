module.exports = {
  start() {
    return {
      type: 'line',
      historyLength: 50,

      label: ' Latitude ',
      showLegend: true,

      line: {
        title: 'Lat s',
        style: { line: 'red' }
      }
    };
  },
  processLine(navData) {
    // 45.2168311 (25.2168)

    return convertCoordinate(navData.gps.latitude).fullSeconds * 1e3;
  }
};


function convertCoordinate(fullHours) {
  let hours = Math.floor(fullHours);
  let fullMinutes = (fullHours - hours) * 60;
  let minutes = Math.floor(fullMinutes);
  let fullSeconds = (fullMinutes - minutes) * 60;
  let seconds = Math.floor(fullSeconds);
  let milliseconds = (fullSeconds - seconds) * 1e3;

  return { hours, minutes, seconds, milliseconds, fullHours, fullMinutes, fullSeconds };
}
