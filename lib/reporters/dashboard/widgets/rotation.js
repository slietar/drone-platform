module.exports = {
  start() {
    return {
      type: 'line',
      historyLength: 50,

      label: ' Rotation ',
      xLabelPadding: 3,
      xPadding: 5,
      showLegend: true,
      wholeNumbersOnly: true,
      style: { text: 'green', baseline: 'cyan' },

      lines: [
        { title: 'X',
          label: 'x',
          style: { line: 'red' } },
        { title: 'Y',
          label: 'y',
          style: { line: 'yellow' } },
        { title: 'Z',
          label: 'z',
          style: { line: 'green' } }
      ]
    };
  },
  processLines(navData) {
    return {
      x: navData.rotation.x + 180,
      y: navData.rotation.y + 180,
      z: navData.rotation.z + 180
    };
  }
};
