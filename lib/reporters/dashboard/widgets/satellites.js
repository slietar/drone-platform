module.exports = {
  start() {
    return {
      type: 'bar',

      label: ' Satellites ',
      barWidth: 4,
      maxHeight: 40,
      xLabelPadding: 3,
      xPadding: 5,
      showLegend: true,
      wholeNumbersOnly: true,
      style: { text: 'green' }
    };
  },
  update(navData) {
    let data = {
      titles: [],
      data: []
    };

    for (let satellite of navData.gps.satellites) {
      data.titles.push(`S${satellite.id}`);
      data.data.push(satellite.signalStrength);
    }

    return (widget) => widget.setData(data);
  }
};
