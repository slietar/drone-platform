module.exports = {
  start() {
    return {
      type: 'bar',

      label: ' Motors ',
      barWidth: 4,
      maxHeight: 40,
      xLabelPadding: 3,
      xPadding: 5,
      showLegend: true,
      wholeNumbersOnly: true,
      style: { text: 'green' }
    };
  },
  update(navData) {
    let data = {
      titles: [],
      data: []
    };

    navData.pwm.motors.forEach((power, index) => {
      data.titles.push(`M${index}`);
      data.data.push(power);
    });

    return (widget) => widget.setData(data);
  }
};
