module.exports = {
  start() {
    return {
      type: 'log',

      label: ' Flight log ',
      fg: 'green',
      selectedFg: 'green'
    };
  },
  log(logs) {
    return (widget) => {
      for (let log of logs) {
        widget.log(log.message);
      }
    };
  }
};
