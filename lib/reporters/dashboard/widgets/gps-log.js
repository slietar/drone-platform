module.exports = {
  start() {
    return {
      type: 'log',

      label: ' GPS Log ',
      fg: 'green',
      selectedFg: 'green'
    };
  },
  update(navData) {
    // HH . MM SS SSS IIIII
    //

    // old 45.2168311 5.8069027 -> 45°13'00.6"N 5°48'24.9"E
    // 45.2169088 : 45°13'1.199999999995498"N 5°48'1.199999999995498"E
    // 45.2170158 : 45°13'1.199999999995498"N 5°48'1.199999999995498"E

    // 45.2171767 : 45°13'1.8361200000111921"N 5°48'1.8361200000111921"E
    // 45.2159377 : 45°12'57.376"N 5°48'57.376"E

    let lat = convertCoordinate(navData.gps.latitude);
    let lon = convertCoordinate(navData.gps.longitude);

    return (widget) => {
      let formattedLat = `${lat.hours}°${lat.minutes}'${lat.fullSeconds.toFixed(3)}"N`;
      let formattedLng = `${lon.hours}°${lon.minutes}'${lon.fullSeconds.toFixed(3)}"E`;

      widget.log(`${formattedLat} ${formattedLng}`);
    };
  }
};


function convertCoordinate(fullHours) {
  let hours = Math.floor(fullHours);
  let fullMinutes = (fullHours - hours) * 60;
  let minutes = Math.floor(fullMinutes);
  let fullSeconds = (fullMinutes - minutes) * 60;
  let seconds = Math.floor(fullSeconds);
  let milliseconds = (fullSeconds - seconds) * 1e3;

  return { hours, minutes, seconds, milliseconds, fullHours, fullMinutes, fullSeconds };
}
