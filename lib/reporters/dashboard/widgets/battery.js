module.exports = {
  start() {
    return {
      type: 'gauge',

      label: ' Battery ',
      stroke: 'green',
      fill: 'white'
    };
  },
  update(navData) {
    return (widget) => widget.setPercent(navData.battery);
  }
};
