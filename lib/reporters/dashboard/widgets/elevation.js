module.exports = {
  start() {
    return {
      type: 'line',
      historyLength: 50,

      label: ' Elevation ',

      line: {
        title: 'Elevation',
        style: { line: 'red' }
      }
    };
  },
  processLine(navData) {
    return navData.gps.elevation;
  }
};
