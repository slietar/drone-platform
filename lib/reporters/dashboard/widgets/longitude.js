module.exports = {
  start() {
    return {
      type: 'line',
      historyLength: 50,

      label: ' Longitude ',
      showLegend: true,

      line: {
        title: 'Lon s',
        style: { line: 'red' }
      }
    };
  },
  processLine(navData) {
    // 5.8069027 (5.8069)

    return convertCoordinate(navData.gps.latitude).fullSeconds * 1e3;
  }
};


function convertCoordinate(fullHours) {
  let hours = Math.floor(fullHours);
  let fullMinutes = (fullHours - hours) * 60;
  let minutes = Math.floor(fullMinutes);
  let fullSeconds = (fullMinutes - minutes) * 60;
  let seconds = Math.floor(fullSeconds);
  let milliseconds = (fullSeconds - seconds) * 1e3;

  return { hours, minutes, seconds, milliseconds, fullHours, fullMinutes, fullSeconds };
}
