const blessed = require('blessed');
const contrib = require('blessed-contrib');

const Reporter = require('../');

const allWidgetsOptions = [
  { generator: require('./widgets/acceleration'), position: [0, 6, 3, 6] },
  { generator: require('./widgets/battery'), position: [0, 0, 3, 3] },
  { generator: require('./widgets/flight-log'), position: [6, 0, 3, 6] },
  { generator: require('./widgets/gps-log'), position: [9, 0, 3, 6] },
  { generator: require('./widgets/gyroscope'), position: [3, 6, 3, 6] },
  { generator: require('./widgets/motors'), position: [0, 3, 3, 3] },
  { generator: require('./widgets/rotation'), position: [6, 6, 3, 6] },
  { generator: require('./widgets/satellites'), position: [3, 0, 3, 6] },
  { generator: require('./widgets/velocity'), position: [9, 6, 3, 6] }
];


class DashboardReporter extends Reporter {
  constructor(widgetsOptions = allWidgetsOptions) {
    super();

    this.allNavData = [];
    this.intervalIds = [];
    this.latestNavData = null;
    this.logs = [];

    this.widgets = widgetsOptions.map((widgetOptions) => new Widget(widgetOptions, this.grid));
  }

  log(log) {
    this.logs.push(log);

    for (let widget of this.widgets) {
      widget.log(log);
    }
  }

  render() {
    for (let widget of this.widgets) {
      widget.render(this.allNavData);
    }

    this.screen.render();
  }

  start() {
    let Grid = contrib.grid;

    this.screen = blessed.screen();
    this.grid = new Grid({ rows: 12, cols: 12, screen: this.screen });

    for (let widget of this.widgets) {
      widget.start(this.grid);
    }

    this.intervalIds.push(setInterval(() => {
      this.render();
    }, 50));

    this.intervalIds.push(setInterval(() => {
      this.writeNavData();
    }, 50));
  }

  stop() {
    this.intervalIds.map(clearInterval);
  }

  update(navData) {
    this.latestNavData = navData;
  }

  writeNavData() {
    let navData = this.latestNavData;

    if (!navData) {
      return;
    }

    this.latestNavData = null;
    this.allNavData.push(navData);

    for (let widget of this.widgets) {
      widget.update(navData);
    }
  }
}


class Widget {
  constructor(options) {
    this.context = {};
    this.generator = options.generator;
    this.logs = [];
    this.options = options;
    this.values = [];
  }

  log(log) {
    this.logs.push(log);
  }

  render(allNavData) {
    let historyLength = this.options.historyLength || 30;

    switch (this.type) {
      case 'line':
        this.blessedWidget.setData([{
          title: this.blessedWidgetOptions.line.title,
          x: fillArray(historyLength),
          y: this.values.slice(-historyLength),
          style: this.blessedWidgetOptions.line.style
        }]);
        break;
      case 'lines':
        this.blessedWidget.setData(
          this.blessedWidgetOptions.lines.map((lineOptions) => ({
            title: lineOptions.title,
            x: fillArray(historyLength),
            y: this.values.slice(-historyLength).map((values) => values[lineOptions.label]),
            style: lineOptions.style
          }))
        );
        break;
      case 'log':
        Reflect.apply(this.generator.log, this.context, [this.logs])(this.blessedWidget);
        this.logs = [];
        break;
      case 'update':
        if (allNavData.length < 1) {
          break;
        }

        Reflect.apply(
          this.generator.update,
          this.context,
          [allNavData.slice(-1)[0], allNavData.slice(-historyLength)]
        )(this.blessedWidget);
    }
  }

  start(grid) {
    this.blessedWidgetOptions = Reflect.apply(this.generator.start, this.context, []);
    this.blessedWidget = grid.set(
      ...this.options.position,
      contrib[this.blessedWidgetOptions.type],
      this.blessedWidgetOptions
    );
  }

  get type() {
    if (this.generator.processLines) {
      return 'lines';
    } if (this.generator.processLine) {
      return 'line';
    } if (this.generator.log) {
      return 'log';
    }

    return 'update';
  }

  update(navData) {
    switch (this.type) {
      case 'line':
        this.values.push(Reflect.apply(this.generator.processLine, this.context, [navData]));
        break;
      case 'lines':
        this.values.push(Reflect.apply(this.generator.processLines, this.context, [navData]));
    }
  }
}


function fillArray(length) {
  let arr = [];
  for (let i = 0; i < length; i++) {
    arr.push(i);
  }
  return arr;
}


module.exports = DashboardReporter;
