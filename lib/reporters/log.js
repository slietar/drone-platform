const Reporter = require('./');


class LogReporter extends Reporter {
  log(log) {
    console.log(`[${log.flight.id}] ${log.message}`);
  }
}


module.exports = LogReporter;
