class Reporter {
  constructor() {
    this.flight = null;
  }

  begin(flight) {
    this.flight = flight;
  }

  end() {

  }

  log() {

  }

  start() {

  }

  stop() {

  }

  update() {

  }
}


module.exports = Reporter;
