const http = require('http');

const Reporter = require('./');


class RealtimeMapReporter extends Reporter {
  constructor() {
    super();

    this.navData = null;

    this.position = {
      latitude: 0,
      longitude: 0
    };

    this.inferedNextPosition = {
      latitude: 0,
      longitude: 0
    };

    this.lastNotedPosition = {
      latitude: 0,
      longitude: 0
    };

    this.positions = [];
    this.intervalId = -1;

    this.server = http.createServer((req, res) => {
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Access-Control-Request-Method', '*');
      res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
      res.setHeader('Access-Control-Allow-Headers', '*');

      res.write(JSON.stringify(this.geoJSON));
      res.end();
    });
  }

  begin() {
    this.server.listen(3212);
  }

  get geoJSON() {
    return {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: { id: 'position' },
          geometry: {
            type: 'Point',
            coordinates: [this.position.longitude, this.position.latitude]
          }
        },
        {
          type: 'Feature',
          properties: {
            id: 'path',
            style: { weight: 3 }
          },
          geometry: {
            type: 'LineString',
            coordinates: this.positions.map((position) => [position.longitude, position.latitude])
          }
        },
        {
          type: 'Feature',
          properties: {
            id: 'infered_next_position',
            style: { dashArray: '2, 10', weight: 3 }
          },
          geometry: {
            type: 'LineString',
            coordinates: [
              [this.position.longitude, this.position.latitude],
              [this.inferedNextPosition.longitude, this.inferedNextPosition.latitude]
            ]
          }
        },
        {
          type: 'Feature',
          properties: {
            id: 'position_cross',
            style: { weight: 1 }
          },
          geometry: {
            type: 'MultiLineString',
            coordinates: [
              [
                [this.position.longitude - 1e-2, this.position.latitude],
                [this.position.longitude + 1e-2, this.position.latitude]
              ],
              [
                [this.position.longitude, this.position.latitude - 1e-2],
                [this.position.longitude, this.position.latitude + 1e-2]
              ]
            ]
          }
        }
      ]
    };
  }

  stop() {
    this.server.close();
    clearInterval(this.intervalId);
  }

  update(navData) {
    this.navData = navData;

    let lat = this.navData.gps.latitude;
    let lon = this.navData.gps.longitude;

    if (lat === 0 && lon === 0) {
      return;
    }

    let radAngle = navData.rotation.z / 180 * Math.PI;

    this.inferedNextPosition.latitude = lat + Math.cos(radAngle) * 1e-3;
    this.inferedNextPosition.longitude = lon + Math.sin(radAngle) * 1e-3;

    if (lat === this.position.latitude & lon === this.position.longitude) {
      return;
    }

    this.position.latitude = lat;
    this.position.longitude = lon;

    this.positions.push({ longitude: lon, latitude: lat });
  }
}


module.exports = RealtimeMapReporter;
