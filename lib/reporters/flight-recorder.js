const path = require('path');
const protoBuf = require('protobufjs');

const Reporter = require('./');
const fs = require('../mz/fs');


const flightDataProto = path.join(process.cwd(), 'proto/flight-data.proto');

class FlightRecorderReporter extends Reporter {
  constructor() {
    super();

    this.allNavData = [];
    this.logs = [];
  }

  end() {
    let outputFile = path.join(process.cwd(), 'output/flights', this.flight.machineId);

    return fs.readFile(flightDataProto)
      .then((proto) => {
        let builder = protoBuf.loadProto(proto.toString());
        let FlightData = builder.build('FlightData');

        let flightData = new FlightData({
          flight: this.flight.toJSON(),
          logs: this.logs,
          navData: this.allNavData,
          version: 2
        });

        return fs.writeFile(outputFile, flightData.encode().toBuffer());
      });
  }

  log(log) {
    this.logs.push(log.toJSON());
  }

  update(navData) {
    this.allNavData.push(navData.toJSON());
  }
}


module.exports = FlightRecorderReporter;
