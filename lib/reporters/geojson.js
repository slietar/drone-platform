const fs = require('../mz/fs');


class GeoJSONReporter {
  constructor() {
    this.coordinates = [];
  }

  end() {
    if (this.coordinates.length < 1) {
      return;
    }

    let outputFile = `${__dirname}/../../output/geojson/${this.flight.machineId}.json`;

    return fs.writeFile(outputFile, JSON.stringify(this.toGeoJSON()));
  }

  start({ flight }) {
    this.flight = flight;
  }

  toGeoJSON() {
    return {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'LineString',
            coordinates: this.coordinates
          }
        }
      ]
    };
  }

  update(navData) {
    let latitude = navData.gps.latitude;
    let longitude = navData.gps.longitude;

    if (latitude === 0 && longitude === 0
     || latitude === this.prevLatitude && longitude === this.prevLongitude) {
      return;
    }

    this.prevLatitude = latitude;
    this.prevLongitude = longitude;
    this.coordinates.push([longitude, latitude]);
  }
}

module.exports = GeoJSONReporter;
