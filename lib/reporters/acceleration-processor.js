const fs = require('../mz/fs');
const GeoJSONReporter = require('./geojson');


const EarthCircumference = 40074.275; // km

class AccelerationProcessorReporter extends GeoJSONReporter {
  constructor() {
    super();

    this.positions = [];
  }

  _convertLatitude(value) {
    let oneDegree = EarthCircumference
                  * Math.cos(this.firstGpsData.longitude / 180 * Math.PI) / 360;
    return 1 / oneDegree * value;
  }

  _convertLongitude(value) {
    let oneDegree = EarthCircumference * Math.cos(this.firstGpsData.latitude / 180 * Math.PI) / 360;
    return 1 / oneDegree * value;
  }

  end() {
    let outputFile = `${__dirname}/../../output/geojson2/${this.flight.machineId}.json`;

    return fs.writeFile(outputFile, JSON.stringify(this.toGeoJSON()));
  }

  update(navData) {
    let { acceleration, gps, timestamp } = navData;

    if (!this.firstAcceleration) {
      this.firstAcceleration = acceleration;
    } if (!this.firstGpsData) {
      this.firstGpsData = /* { latitude: 45.216844, longitude: 5.807189 }; */ gps;
    } if (!this.lastTimestamp) {
      this.lastTimestamp = timestamp;
    } if (!this.position) {
      this.position = { x: 0, y: 0, z: 0 };
    } if (!this.speed) {
      this.speed = { x: 0, y: 0, z: 0 };
    }

    let duration = timestamp - this.lastTimestamp;
    let deltaT = duration * 1e-3;

    this.lastTimestamp += duration;

    this.speed.x += (acceleration.x - this.firstAcceleration.x) * deltaT;
    this.speed.y += (acceleration.y - this.firstAcceleration.y) * deltaT;
    this.speed.z += (acceleration.z - this.firstAcceleration.z) * deltaT;

    this.position.x += this.speed.x * deltaT;
    this.position.y += this.speed.y * deltaT;
    this.position.z += this.speed.z * deltaT;

    this.positions.push({
      x: this.position.x,
      y: this.position.y,
      z: this.position.z
    });

    // 45.216808, 5.807458
    // 45.216844, 5.807189
    // 45°13'0.385"N 5°48'26.229"E

    this.coordinates.push([
      this.firstGpsData.longitude + this._convertLongitude(this.position.y * 1e-7),
      this.firstGpsData.latitude + this._convertLatitude(this.position.x * 1e-7)
    ]);
  }
}


module.exports = AccelerationProcessorReporter;
