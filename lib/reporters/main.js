class MainReporter {
  constructor(reporters) {
    this.reporters = reporters;
  }

  get begin() {
    return this._runOnReporters('begin');
  }

  get end() {
    return this._runOnReporters('end');
  }

  get log() {
    return this._runOnReporters('log');
  }

  get start() {
    return this._runOnReporters('start');
  }

  get stop() {
    return this._runOnReporters('stop');
  }

  get update() {
    return this._runOnReporters('update');
  }

  _runOnReporters(method) {
    return function (...args) {
      return Promise.all(
        // eslint-disable-next-line no-invalid-this
        this.reporters.map((reporter) => reporter[method](...args))
      );
    };
  }
}


module.exports = MainReporter;
