const Pilot = require('./');


class IdleOnGroundPilot extends Pilot {
  start() {
    return Promise.defer().promise;
  }
}


module.exports = IdleOnGroundPilot;
