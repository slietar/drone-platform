const geolib = require('geolib');

const Pilot = require('./');
const Interval = require('../util/interval');
const parseGpx = require('../util/gpx-parser');


class AutoPilot extends Pilot {
  constructor() {
    super();

    this.intervals = new Set();
    this.timeouts = new Set();
  }

  get compassHeading() {
    let rotation = this.drone.navData.rotation.z;
    this.log(`Rotation: ${rotation}°`);

    return rotation;
  }

  captureSpeed() {
    let { latitude: φ0, longitude: λ0 } = this.drone.navData.gps;
    let timestamp0 = Date.now();

    this.log(`Removing ${ρ}°`);

    return {
      end() {
        let { latitude: φ1, longitude: λ1 } = pilot.drone.navData.gps;
        let timestamp1 = Date.now();

        // km/h
        let speed = geolib.getSpeed(
          { latitude: φ0, longitude: λ0, time: timestamp0 },
          { latitude: φ1, longitude: λ1, time: timestamp1 }
        );

        this.log(`Speed: ${speed} km/h`);

        return speed;
      }
    };
  }

  /* α, β, θ, ρ */
  /* latitude φ, longitude λ */

  goToCoordinates2(targetΦ, targetΛ, {
    maxSpeed = 0.2,
    positionMarginOfError = 2 // meters (radius)
  } = {}) {
    let directionCheckInterval = new Interval(() => {
      let { latitude: φ, longitude: λ } = this.drone.navData.gps;
      let α = haversine(
        { latitude: φ, longitude: λ },
        { latitude: targetΦ, longitude: targetΛ }
      );
      let θ = this.drone.navData.rotation.z;
      let β = fixAngle(α - θ);

      this.log(`Target angle: ${α}`);
      this.log(`Needed rotation: ${β}`);

      let frontDiff = Math.cos(β / 180 * Math.PI);
      let rightDiff = Math.sin(β / 180 * Math.PI);

      let maxDiff = Math.max(Math.abs(frontDiff), Math.abs(rightDiff));

      let frontSpeed = frontDiff / maxDiff * maxSpeed;
      let rightSpeed = rightDiff / maxDiff * maxSpeed;
      let leftSpeed = -rightSpeed;

      this.controller.moveFront(frontSpeed);
      this.controller.moveLeft(leftSpeed);
    }, 500);

    let positionCheckInterval = new Interval((interval) => {
      let { latitude: φ, longitude: λ } = this.drone.navData.gps;

      let distanceToTarget = geolib.getDistance(
        { latitude: φ, longitude: λ },
        { latitude: targetΦ, longitude: targetΛ }
      );

      this.log(`Distance to target: ${distanceToTarget}m`);

      if (distanceToTarget <= positionMarginOfError) {
        directionCheckInterval.end();
        interval.end(distanceToTarget);

        this.intervals.delete(directionCheckInterval);
        this.intervals.delete(interval);

        this.controller.stop();

        return;
      }
    }, 200);

    this.intervals.add(directionCheckInterval);
    this.intervals.add(positionCheckInterval);

    return positionCheckInterval.promise;
  }

  rotateToAngle(α, {
    marginOfError = 2, // positive degrees (± 0.56%)
    maxSlowAngle = 45, // positive degrees
    maxSpeed = 1, // %
    rotationCheckDelay = 100, // ms
    slowSpeed = 0.4 // %
  } = {}) {
    let rotationSpeed = null;

    let interval = new Interval((interval) => {
      let β = fixAngle(α - this.drone.navData.rotation.z);

      if (Math.abs(β) <= marginOfError) {
        interval.end(β);
        this.intervals.delete(interval);

        if (rotationSpeed !== null) {
          this.controller.rotate(0);
        }

        this.log(`Rotation done with ${β}° missing`);

        return;
      }

      this.log(`Missing ${β}°`);

      let rotationSpeedTarget = (() => {
        if (β > maxSlowAngle) {
          return maxSpeed;
        } if (β > 0) {
          return slowSpeed;
        } if (β < -maxSlowAngle) {
          return -maxSpeed;
        }

        return -slowSpeed;
      })();

      if (rotationSpeed !== rotationSpeedTarget) {
        rotationSpeed = rotationSpeedTarget;
        this.controller.rotate(rotationSpeedTarget);
      }
    }, rotationCheckDelay);

    this.intervals.add(interval);

    return interval.promise;
  }

  rotateToRelativeAngle(θ, options) {
    return this.rotateToAngle(this.drone.navData.rotation.z + θ, options);
  }

  runTrack(points) {
    return points.reduce((promise, point, index) =>
      promise
        .then(() => this.controller.moveUp(1, 1e3))
        .then(() => wait(2000))
        .then(() => this.goToCoordinates2(point.latitude, point.longitude))
        .then(() => wait(1000))
        .then(() => this.controller.takePhoto(`${this.flight.machineId}-${index}.png`))
        .then(() => wait(3000))
    , Promise.resolve());
  }

  start() {
    let { latitude: φ, longitude: λ } = this.drone.navData.gps;

    if (φ === 0 || λ === 0) {
      this.log('No GPS data');
      return;
    }

    return Promise.resolve()
      .then(() => {
        this.controller.trimHorizontalRotation();

        return Promise.all([
          parseGpx('0.gpx'),
          this.controller.takeOff()
        ]);
      })
      .then(([track]) => this.runTrack(track))
      .then(() => wait(3000))
      .then(() => this.controller.land());
  }

  stop() {
    for (let interval of this.intervals) {
      if (interval instanceof Interval) {
        interval.end();
      } else {
        clearInterval(interval);
      }
    }

    for (let timeout of this.timeouts) {
      clearTimeout(timeout);
    }

    this.controller.stop();

    return this.controller.land();
  }
}


function fixAngle(angle) {
  if (angle > 180) {
    return angle - 360;
  } if (angle < -180) {
    return angle + 360;
  }

  return angle;
}

function haversine(p0, p1) {
  let dLng = p1.longitude - p0.longitude;
  let x = Math.cos(p0.latitude) * Math.sin(p1.latitude)
        - Math.sin(p0.latitude) * Math.cos(p1.latitude) * Math.cos(dLng);
  let y = Math.sin(dLng) * Math.cos(p1.latitude);

  return Math.atan2(y, x) / Math.PI * 180;
}

function wait(delay) {
  return new Promise((resolve) => {
    setTimeout(resolve, delay);
  });
}


module.exports = AutoPilot;
