/* eslint-disable */

const geolib = require('geolib');

const Pilot = require('./');
const Interval = require('../util/interval');


const Constants = {
  farestAllowedTarget: 1e-3, // degrees (lat, lon)
  movementMarginOfError: 3 // meters (circle radius / 5e-5 degrees)
};


class AutoPilot extends Pilot {
  constructor() {
    super();

    this._compassHeading = null;
    this._compassRotationInitial = 0;

    this.intervals = new Set();
    this.timeouts = new Set();
  }

  get compassHeading() {
    this.log(`Rotation: ${this.drone.navData.rotation.z}°`);

    return this.drone.navData.rotation.z;

    /* if (this._compassHeading === null) {
      return null;
    }

    let correction = this.drone.navData.rotation.z - this._compassRotationInitial;
    this.log(`Corrected ${correction}° from compass heading`);

    return this._compassHeading + correction; */
  }

  set compassHeading(value) {
    this._compassRotationInitial = this.drone.navData.rotation.z;
    this._compassHeading = value;
  }

  captureAngle(speed = 0.3, delay = 5000) {
    let deferred = Promise.defer();
    let capture = this.captureAngleTimelessly();

    if (speed > 0) {
      this.controller.moveFront(speed);
    }

    let timeout = setTimeout(() => {
      this.timeouts.delete(timeout);

      if (speed > 0) {
        this.controller.moveFront(0);
      }

      deferred.resolve(capture.end());
    }, delay);

    this.timeouts.add(timeout);

    return deferred.promise;
  }

  captureAngleTimelessly({
    frontSpeed = 1,
    leftSpeed = 0
  } = {}) {
    let pilot = this;
    let deferred = Promise.defer();
    let { latitude: φ0, longitude: λ0 } = this.drone.navData.gps;
    let timestamp0 = Date.now();

    // angle correction due to not going front
    let ρ = Math.atan2(-leftSpeed, frontSpeed) / Math.PI * 180;

    this.log(`Removing ${ρ}°`);

    return {
      end() {
        let { latitude: φ1, longitude: λ1 } = pilot.drone.navData.gps;
        let timestamp1 = Date.now();
        // let θ = Math.atan2(λ1 - λ0, φ1 - φ0) / Math.PI * 180;

        let θ = haversine(
          { latitude: φ0, longitude: λ0 },
          { latitude: φ1, longitude: λ1 }
        );

        // apply angle correction
        θ -= ρ;

        // km/h
        let speed = geolib.getSpeed(
          { latitude: φ0, longitude: λ0, time: timestamp0 },
          { latitude: φ1, longitude: λ1, time: timestamp1 }
        );

        pilot.compassHeading = θ;

        return θ;
      }
    };
  }

  /* α, β, θ, ρ */
  /* latitude φ, longitude λ */
  goToCoordinates(targetΦ, targetΛ, {
    positionCheckInterval = 200,
    rotationCheckFrameFrequency = 20,
    speed = 0.2
  } = {}) {
    const marginOfError = Constants.movementMarginOfError;

    let correctAngle = (θ) => {
      let { latitude: φ, longitude: λ } = this.drone.navData.gps;

      let α = haversine(
        { latitude: φ, longitude: λ },
        { latitude: targetΦ, longitude: targetΛ }
      );
      let β = fixAngle(α - θ);

      this.log(`Target angle: ${α}`);
      this.log(`Detected angle: ${θ}°`);
      this.log(`Rotating: ${β}°`);

      if (moving) {
        moving = false;
        this.controller.stop();
      }

      return wait(1000)
        .then(() => {
          return this.rotateToRelativeAngle(β);
        });
    };

    let capture;
    let moving = false;

    return Promise.resolve()
      .then(() => {
        if (this.compassHeading) {
          return this.compassHeading;
        }

        return this.captureAngle();
      })
      .then((θ) => {
        return correctAngle(θ);
      })
      .then(() => {
        let interval = new Interval((interval) => {
          let { latitude: φ, longitude: λ } = this.drone.navData.gps;

          if (capture && interval.frame % rotationCheckFrameFrequency < 1) {
            let θ = capture.end();

            return correctAngle(θ);
          }

          capture = this.captureAngleTimelessly();

          let distanceToTarget = geolib.getDistance(
            { latitude: φ, longitude: λ },
            { latitude: targetΦ, longitude: targetΛ }
          );

          // this.log(`Distance to target: ${distanceToTarget}m`);

          if (distanceToTarget <= marginOfError) {
            interval.end();
            this.intervals.delete(interval);

            if (moving) {
              moving = false;
              this.controller.stop();
            }

            return;
          }

          if (!moving) {
            moving = true;
            this.controller.moveFront(speed);
          }
        }, positionCheckInterval);

        this.intervals.add(interval);

        return interval.promise;
      });
  }

  goToCoordinates2(targetΦ, targetΛ, {
    maxSpeed = 0.2,
    positionMarginOfError = 2, // meters (radius)
  } = {}) {
    if (!this.compassHeading) {
      return this.captureAngle()
        .then(() => {
          return this.goToCoordinates2(targetΦ, targetΛ, { maxSpeed, positionMarginOfError });
        });
    }

    let θ = this.compassHeading;
    this.log(`Angle: ${θ}`);

    let directionCheckInterval = new Interval(() => {
      let { latitude: φ, longitude: λ } = this.drone.navData.gps;
      let α = haversine(
        { latitude: φ, longitude: λ },
        { latitude: targetΦ, longitude: targetΛ }
      );
      let θ = this.compassHeading;
      let β = fixAngle(α - θ);

      this.log(`Target angle: ${α}`);
      this.log(`Needed rotation: ${β}`);

      let frontDiff = Math.cos(β / 180 * Math.PI);
      let rightDiff = Math.sin(β / 180 * Math.PI);

      let maxDiff = Math.max(Math.abs(frontDiff), Math.abs(rightDiff));

      let frontSpeed = frontDiff / maxDiff * maxSpeed;
      let rightSpeed = rightDiff / maxDiff * maxSpeed;
      let leftSpeed = -rightSpeed;

      this.controller.moveFront(frontSpeed);
      this.controller.moveLeft(leftSpeed);
    }, 500);

    let positionCheckInterval = new Interval((interval) => {
      let { latitude: φ, longitude: λ } = this.drone.navData.gps;

      let distanceToTarget = geolib.getDistance(
        { latitude: φ, longitude: λ },
        { latitude: targetΦ, longitude: targetΛ }
      );

      this.log(`Distance to target: ${distanceToTarget}m`);

      if (distanceToTarget <= positionMarginOfError) {
        directionCheckInterval.end();
        interval.end(distanceToTarget);

        this.intervals.delete(directionCheckInterval);
        this.intervals.delete(interval);

        this.controller.stop();

        return;
      }
    }, 200);

    this.intervals.add(directionCheckInterval);
    this.intervals.add(positionCheckInterval);

    return positionCheckInterval.promise;
  }

  rotateToAngle(α, {
    marginOfError = 2, // positive degrees (± 0.56%)
    maxSlowAngle = 45, // positive degrees
    maxSpeed = 1, // %
    rotationCheckDelay = 100, // ms
    slowSpeed = 0.4 // %
  } = {}) {
    let rotationSpeed = null;

    let interval = new Interval((interval) => {
      let β = fixAngle(α - this.drone.navData.rotation.z);

      if (Math.abs(β) <= marginOfError) {
        interval.end(β);
        this.intervals.delete(interval);

        if (rotationSpeed !== null) {
          this.controller.rotate(0);
        }

        this.log(`Rotation done with ${β}° missing`);

        return;
      }

      this.log(`Missing ${β}°`);

      let rotationSpeedTarget = (() => {
        if (β > maxSlowAngle) return maxSpeed;
        if (β > 0) return slowSpeed;
        if (β < -maxSlowAngle) return -maxSpeed;
        return -slowSpeed;
      })();

      if (rotationSpeed !== rotationSpeedTarget) {
        rotationSpeed = rotationSpeedTarget;
        this.controller.rotate(rotationSpeedTarget);
      }
    }, rotationCheckDelay);

    this.intervals.add(interval);

    return interval.promise;
  }

  rotateToRelativeAngle(θ, options) {
    return this.rotateToAngle(this.drone.navData.rotation.z + θ, options);
  }

  runTrack(points) {
    return points.reduce((promise, point) => {
      return promise
        .then(() => {
          return wait(2000);
        })
        .then(() => {
          return this.goToCoordinates2(point.latitude, point.longitude);
        });
    }, Promise.resolve());
  }

  start() {
    let { latitude: φ, longitude: λ } = this.drone.navData.gps;

    if (φ === 0 || λ === 0) {
      this.log('No GPS data');
      return;
    }

    return Promise.resolve()
      .then(() => {
        this.controller.client.ftrim();

        return this.controller.takeOff();
      })
      .then(() => {
        return this.controller.moveUp(0.5, 2000);
      })
      .then(() => {
        // return this.captureAngle();
        // this.drone.client.calibrate(0);
        // return wait(15000);
      })
      .then(() => {
        return wait(2000);
      })
      .then(() => {
        return this.goToCoordinates2(45.2164783, 5.8080034);
      })
      .then(() => {
        return wait(2000);
      })
      .then(() => {
        return this.goToCoordinates2(45.216557, 5.807587);
      })
      .then(() => {
        return wait(2000);
      })
      .then(() => {
        return this.controller.land();
      });

    return this.controller.takeOff()
      .then(() => {
        return this.captureAngle()
          .then((ang) => {
            this.log(`Captured angle: ${ang}`);
            return this.controller.land();
          });
      });

    return this.controller.takeOff()
      .then(() => {
        // return this.controller.moveUp(1, 0.5e3);
      })
      .then(() => {
        return wait(3000);
      })
      .then(() => {
        return this.goToCoordinates2(45.2164783, 5.8080034);
      })
      .then(() => {
        return wait(3000);
      })
      .then(() => {
        return this.goToCoordinates2(45.216557, 5.807587);
      })
      .then(() => {
        return wait(3000);
      })
      .then(() => {
        return this.controller.land();
      });

    let capture;

    return this.controller.takeOff()
      .then(() => {
        return wait(3000);
      })
      .then(() => {
        capture = this.captureAngleTimelessly({
          frontSpeed: 1,
          leftSpeed: 1
        });

        this.controller.moveFront(0.2);
        this.controller.moveLeft(0.2);

        return wait(5000);
      })
      .then(() => {
        this.controller.stop();
        let ang = capture.end();

        this.log(`Angle: ${ang}`);

        return wait(3000);
      })
      .then(() => {
        return this.controller.land();
      });

    return wait(5000)
      .then(() => {
        let ang = capture.end();

        this.log(`Angle: ${ang}`);
      });

    return this.controller.takeOff()
      .then(() => {
        return new Promise((r) => setTimeout(r, 3000));
      })
      .then(() => {
        return this.goToCoordinates2(45.2164930, 5.8080071);
        return this.goToCoordinates(45.2164930, 5.8080071);
        return this.goToCoordinates(45.2164642, 5.8081273)
      })
      .then(() => {
        return this.controller.land();
      });

    return this.goToCoordinates2(45.216557, 5.808021);
    return this.goToCoordinates2(45.216967, 5.807093);
  }

  stop() {
    for (let interval of this.intervals) {
      if (interval instanceof Interval) {
        interval.end();
      } else {
        clearInterval(interval);
      }
    }

    for (let timeout of this.timeouts) {
      clearTimeout(timeout);
    }

    this.controller.stop();

    return this.controller.land();
  }
}


function fixAngle(angle) {
  if (angle > 180) {
    return angle - 360;
  } if (angle < -180) {
    return angle + 360;
  }

  return angle;
}

function haversine(p0, p1) {
  let dLng = p1.longitude - p0.longitude;
  let x = Math.cos(p0.latitude) * Math.sin(p1.latitude) - Math.sin(p0.latitude) * Math.cos(p1.latitude) * Math.cos(dLng);
  let y = Math.sin(dLng) * Math.cos(p1.latitude);

  return Math.atan2(y, x) / Math.PI * 180;
}

function wait(delay) {
  return new Promise((resolve) => {
    setTimeout(resolve, delay);
  });
}


module.exports = AutoPilot;
