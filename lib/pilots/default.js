const Pilot = require('./');


class DefaultPilot extends Pilot {
  start() {
    return this.controller.animateLeds('blinkRed', 2, 3);
  }
}


module.exports = DefaultPilot;
