const events = require('events');


class Pilot extends events.EventEmitter {
  get controller() {
    return this.drone.controller;
  }

  get drone() {
    return this.flight.drone;
  }

  initialize(flight) {
    this.flight = flight;

    return Promise.resolve();
  }

  get log() {
    return this.flight.log.bind(this.flight);
  }

  start() {
    return Promise.resolve();
  }

  stop() {
    return Promise.resolve();
  }
}


module.exports = Pilot;
