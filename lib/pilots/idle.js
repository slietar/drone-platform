const Pilot = require('./');


class IdlePilot extends Pilot {
  start() {
    return this.controller.takeOff()
      .then(() => Promise.defer().promise);
  }

  stop() {
    return this.controller.land();
  }
}


module.exports = IdlePilot;
