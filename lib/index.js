const parseArgs = require('./cli');
const fly = require('./fly');
const repl = require('./repl');
const replay = require('./replay');


let args = parseArgs();
let command = args._[0];


if (!command) {
  throw new Error('Missing command');
}

switch (command) {
  case 'fly':
    fly(args);
    break;
  case 'repl':
    repl(args);
    break;
  case 'replay':
    replay(args);
    break;
  default:
    throw new Error(`Unknown command '${command}'`);
}
