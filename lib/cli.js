/**
 * AR.Drone
 * CLI
 */


const subarg = require('subarg');


function parseArgs(argv = process.argv.slice(2)) {
  let args = subarg(argv, {
    boolean: ['fast', 'unlocked'],
    string: ['control-tower', 'partial', 'pilot', 'reporter', 'speed', 'wifi'],
    default: {
      'control-tower': 'default',
      partial: 1,
      pilot: 'default',
      reporter: 'presets/default',
      speed: 1,
      unlocked: false,
      wifi: 'auto'
    }
  });

  args.partial = parseFloat(args.partial);
  args.speed = parseFloat(args.speed);

  return args;
}


module.exports = parseArgs;
