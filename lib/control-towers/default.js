const ControlTower = require('./');


class DefaultControlTower extends ControlTower {
  constructor({ unlocked } = {}) {
    super();

    this.locked = !unlocked;
  }

  requestLanding(drone) {
    if (this.locked) {
      return Promise.reject(new Error('Control tower is locked (enable --unlocked option)'));
    }

    return Promise.resolve();
  }

  requestTakeOff(drone) {
    return Promise.resolve()
      .then(() => {
        if (this.locked) {
          throw new Error('Control tower is locked (enable --unlocked option)');
        }

        let state = drone.navData.droneState;

        if (state.anglesOutOfRange) {
          throw new Error('Angles out of range (try resetting the drone controller)');
        } if (state.cutoutDetected) {
          throw new Error('Cutout detected (try resetting the drone controller)');
        } if (state.lowBattery) {
          throw new Error('Low battery');
        } if (state.magnometerNeedsCalibration) {
          throw new Error('Magnometer needs calibration (use the official app)');
        } if (state.motorProblem) {
          throw new Error('Motor problem');
        } if (state.softwareFault) {
          throw new Error('Software problem');
        } if (state.tooMuchWind) {
          throw new Error('Too much wind');
        } if (state.ultrasonicSensorDeaf) {
          throw new Error('Ultrasonic sensor deaf');
        } if (state.userEmergencyLanding) {
          throw new Error('Unknown problem (try resetting the drone controller)');
        }

        return drone.wifiController.controller.getCurrentNetwork();
      })
      .then((currentNetwork) => {
        if (currentNetwork.signal_level < -90) {
          throw new Error(`WiFi signal strength too low (use '--wifi fake' to bypass)`);
        }
      });
  }
}


module.exports = DefaultControlTower;
