const thenify = require('thenify');


module.exports = function (int) {
  return {
    close: int.close.bind(int),
    on: int.on.bind(int),
    pause: int.pause.bind(int),
    question: thenify((question, callback) => {
      int.question(question, (answer) => callback(null, answer));
    }),
    resume: int.resume.bind(int)
  };
};
