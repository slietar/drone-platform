const thenify = require('thenify');
const xml2js = require('xml2js');


exports.parseString = thenify(xml2js.parseString);
