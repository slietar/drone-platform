const thenify = require('thenify');


module.exports = function (client) {
  return {
    animate: client.animate.bind(client),
    animateLeds: client.animateLeds.bind(client),
    back: client.back.bind(client),
    calibrate: client.calibrate.bind(client),
    clockwise: client.clockwise.bind(client),
    createRepl: client.createRepl.bind(client),
    counterClockwise: client.counterClockwise.bind(client),
    disableEmergency: client.disableEmergency.bind(client),
    down: client.down.bind(client),
    front: client.front.bind(client),
    ftrim: client.ftrim.bind(client),
    getPngStream: client.getPngStream.bind(client),
    left: client.left.bind(client),
    on: client.on.bind(client),
    right: client.right.bind(client),
    stop: client.stop.bind(client),
    up: client.up.bind(client),

    config: thenify(client.config.bind(client)),
    land: thenify(client.land.bind(client)),
    takeoff: thenify(client.takeoff.bind(client))
  };
};
