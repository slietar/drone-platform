const fs = require('fs');
const thenify = require('thenify');


exports.readFile = thenify(fs.readFile);
exports.writeFile = thenify(fs.writeFile);
