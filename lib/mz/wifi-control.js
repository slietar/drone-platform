const thenify = require('thenify');
const wifiControl = require('wifi-control');


exports.configure = wifiControl.configure;
exports.findInterface = wifiControl.findInterface;
exports.getIfaceState = wifiControl.getIfaceState;
exports.connectToAP = thenify(wifiControl.connectToAP);
exports.resetWiFi = thenify(wifiControl.resetWiFi);
exports.scanForWiFi = thenify(wifiControl.scanForWiFi);
