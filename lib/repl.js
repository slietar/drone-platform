const readline = require('readline');

const Drone = require('./drone');
const Flight = require('./flight');
const parseArgs = require('./cli');
const mzReadline = require('./mz/readline');


// »

module.exports = function (options) {
  let flight;

  let ControlTower = require(`./control-towers/${options['control-tower']}`);
  let controlTower = new ControlTower({ unlocked: options.unlocked });
  let drone = new Drone(controlTower);

  let rlInt = mzReadline(readline.createInterface({
    input: process.stdin,
    output: process.stdout
  }));

  rlInt.on('SIGINT', () => {
    if (flight) {
      flight.stop();
    } else {
      drone.disconnect()
        .then(() => {
          process.exit();
        })
        .catch((err) => {
          console.error(err.stack);
          process.exit(1);
        });
    }
  });

  let loop = () => {
    rlInt.resume();

    let reporter;

    return rlInt.question('» ')
      .then((fullCommand) => {
        // rlInt.pause();

        let options = parseArgs(fullCommand.split(' '));
        let command = options._[0];

        if (!command) {
          return;
        } if (command === 'exit') {
          return { stopLoop: true };
        } if (command !== 'fly') {
          throw new Error(`Unknown command '${command}'`);
        }

        let Pilot = require(`./pilots/${options.pilot}`);
        let Reporter = require(`./reporters/${options.reporter}`);

        let pilot = new Pilot();

        flight = new Flight({ drone, pilot });
        reporter = new Reporter();

        return reporter.begin(flight)
          .then(() => reporter.start())
          .then(() => {
            flight.on('log', (log) => {
              reporter.log(log);
            });

            return Promise.all([
              flight.initialize(),
              drone.controller.reset()
            ]);
          })
          .then(() => {
            let deferred = Promise.defer();

            drone.on('navData', (navData) => {
              reporter.update(navData);
            });

            let exitFlight = () => {
              deferred.resolve(
                reporter.end()
                  .then(() => reporter.stop())
              );
            };

            flight.on('end', exitFlight);
            flight.on('stop', exitFlight);

            flight.start();

            return deferred.promise;
          })
          .then(() => {
            drone.removeAllListeners();

            flight = null;
          })
          .catch((err) => {
            console.error(err.stack);

            flight.stop();
          });
      })
      .catch((err) => {
        console.error(err.stack);
      })
      .then(({ stopLoop } = {}) => {
        if (!stopLoop) {
          return loop();
        }
      });
  };

  drone.connect(options.wifi)
    .then(() => new Promise((resolve) => setTimeout(resolve, 5000)))
    .then(() => drone.initialize())
    .then(() => loop())
    .then(() => {
      rlInt.close();

      return drone.disconnect();
    })
    .then(() => {
      process.exit();
    })
    .catch((err) => {
      rlInt.close();

      console.error(err);
      process.exit(1);
    });
};
