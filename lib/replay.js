const protoBuf = require('protobufjs');

const constants = require('./drone/constants');
const fs = require('./mz/fs');


module.exports = function replay(options) {
  let Reporter = require(`./reporters/${options.reporter}`);

  let flightDataProtoFile = `${__dirname}/../proto/flight-data.proto`;
  let flightDataFile = `${__dirname}/../output/flights/${options._[1]}`;

  let reporter = new Reporter();
  let flightData;

  const speedRatio = 1 / options.speed;

  Promise.all([
    fs.readFile(flightDataProtoFile),
    fs.readFile(flightDataFile)
  ])
    .then(([proto, encodedFlightData]) => {
      let builder = protoBuf.loadProto(proto);
      let FlightData = builder.build('FlightData');

      flightData = FlightData.decode(encodedFlightData);

      for (let navData of flightData.navData) {
        // fix enums
        navData.droneState.controlState = constants.CONTROL_STATES[navData.droneState.controlState];
        navData.droneState.flyState = constants.FLY_STATES[navData.droneState.flyState];

        // convert 'long' into 'int'
        navData.timestamp = parseInt(navData.timestamp.toString());
      }

      for (let log of flightData.logs) {
        log.flight = flightData.flight;
        log.timestamp = parseInt(log.timestamp.toString());
      }

      return reporter.begin(flightData.flight);
    })
    .then(() => reporter.start())
    .then(() => {
      let allNavData = flightData.navData;

      if (options.fast) {
        for (let navData of allNavData) {
          reporter.update(navData);
        }

        for (let log of allNavData) {
          reporter.log(log);
        }

        return;
      }

      let iterate = (obj, method) => {
        let deferred = Promise.defer();

        let internalIterate = (dataObjIndex = 0) => {
          if (options.partial >= 1) {
            let rest = dataObjIndex % options.partial;

            if (rest > 0) {
              internalIterate(dataObjIndex + 1);
              return;
            }
          }

          let dataObj = obj[dataObjIndex];

          if (options.partial < 1 || dataObjIndex % options.partial < 1) {
            reporter[method](dataObj);
          }

          let nextDataObjIndex = dataObjIndex + 1;
          let nextDataObj = obj[nextDataObjIndex];

          if (!nextDataObj) {
            deferred.resolve();
            return;
          }

          setTimeout(() => {
            internalIterate(nextDataObjIndex);
          }, (nextDataObj.timestamp - dataObj.timestamp) * speedRatio);
        };

        internalIterate();

        return deferred.promise;
      };

      return Promise.all([
        iterate(flightData.navData, 'update'),
        iterate(flightData.logs, 'log')
      ]);
    })
    .then(() => reporter.stop())
    .then(() => reporter.end())
    .then(() => {
      process.exit(0);
    })
    .catch((err) => {
      console.error(err.stack);
      process.exit(1);
    });


  let triedExiting = false;

  process.on('SIGINT', () => {
    if (triedExiting) {
      process.exit(1);
      return;
    }

    triedExiting = true;

    reporter.end()
      .then(() => {
        process.exit();
      })
      .catch((err) => {
        console.error(err.stack);
        process.exit(1);
      });
  });
};
