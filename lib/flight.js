const events = require('events');


const FlightStatus = {
  uninitialized: 0,
  beingInitialized: 1,
  initialized: 2,
  started: 3,
  ended: 4,
  beingStopped: 5,
  stopped: 6
};

// maximum
// 1468364400 - 1466722800
// xxx1641600 (190c80)
const flightId0Timestamp = 1466722800000;

class Flight extends events.EventEmitter {
  constructor({ drone, pilot }) {
    super();

    this.drone = drone;
    this.pilot = pilot;

    let numId = Math.floor((Date.now() - flightId0Timestamp) * 1e-3)
      .toString(16)
      .toUpperCase();

    this.id = `ESM ${numId}`;
    this.logs = [];

    this.status = FlightStatus.uninitialized;
  }

  initialize() {
    this.status = FlightStatus.beingInitialized;

    this.drone.controller.on('command', (cmd) => {
      if (this.status !== FlightStatus.started) {
        return;
      }

      this.log(cmd);
    });

    this.log(`Flight ID: ${this.id}`);

    return Promise.all([
      this.logWiFiInformation(),
      this.logPosition(),
      this.pilot.initialize(this)
    ])
      .then(() => {
        this.status = FlightStatus.initialized;
      });
  }

  end() {
    if (this.status !== FlightStatus.started) {
      return Promise.resolve();
    }

    this.status = FlightStatus.ended;

    this.log('Flight ended');
    this.emit('end');
  }

  log(message) {
    let log = {
      flight: this,
      message,
      timestamp: Date.now(),
      type: 'default',
      toJSON() {
        return {
          message: this.message,
          timestamp: this.timestamp
        };
      }
    };

    this.logs.push(log);
    this.emit('log', log);
  }

  logPosition() {
    let { latitude, longitude } = this.drone.navData.gps;

    if (latitude > 0 && longitude > 0) {
      this.log(`Position: (lat) ${latitude}° / (lon) ${longitude}°`);
    } else {
      this.log('Warning: no GPS data');
    }
  }

  logWiFiInformation() {
    return this.drone.wifiController.controller.getCurrentNetwork()
      .then((currentNetwork) => {
        this.log(`Connected to '${currentNetwork.ssid}'`);
        this.log(`  MAC address: ${currentNetwork.mac}`);
        this.log(`  Signal strength: ${currentNetwork.signal_level}`);

        this.log(`Battery level: ${this.drone.navData.battery}%`);
      });
  }

  get machineId() {
    return this.id
      .toLowerCase()
      .replace(' ', '-');
  }

  start() {
    this.status = FlightStatus.started;
    this.emit('start');

    return Promise.resolve(this.pilot.start())
      .then(() => {
        this.end();
      })
      .catch((err) => {
        this.log('Aborting flight because of an error');

        for (let line of err.stack.split('\n')) {
          this.log(line);
        }

        this.stop();
      });
  }

  stop() {
    if (this.status === FlightStatus.beingStopped) {
      this.emit('stop');
    } if (this.status !== FlightStatus.started) {
      return Promise.resolve();
    }

    this.status = FlightStatus.beingStopped;

    return Promise.resolve(this.pilot.stop())
      .then(() => {
        this.status = FlightStatus.stopped;

        this.log('Flight stopped');
        this.emit('stop');
      });
  }

  toJSON() {
    return {
      id: this.id,
      machineId: this.machineId
    };
  }
}


module.exports = Flight;
