const Drone = require('./drone');
const Flight = require('./flight');


module.exports = function fly(options) {
  let ControlTower = require(`./control-towers/${options['control-tower']}`);
  let Pilot = require(`./pilots/${options.pilot}`);
  let Reporter = require(`./reporters/${options.reporter}`);

  let controlTower = new ControlTower({ unlocked: options.unlocked });
  let drone = new Drone(controlTower);
  let pilot = new Pilot();
  let reporter = new Reporter();
  let flight;


  drone.connect(options.wifi)
    .then(() => new Promise((resolve) => setTimeout(resolve, 5000)))
    .then(() => drone.initialize())
    .then(() => {
      flight = new Flight({ drone, pilot });

      flight.on('log', (log) => {
        reporter.log(log);
      });

      return reporter.begin(flight);
    })
    .then(() => reporter.start())
    .then(() => flight.initialize())
    .then(() => {
      drone.on('navData', (navData) => {
        reporter.update(navData);
      });

      let exitFlight = () => {
        Promise.all([
          drone.disconnect(),
          reporter.stop()
            .then(() => reporter.end())
        ])
          .then(() => {
            process.exit();
          })
          .catch((err) => {
            console.error(err.stack);
            process.exit(1);
          });
      };

      flight.on('end', exitFlight);
      flight.on('stop', exitFlight);

      flight.start();
    })
    .catch((err) => {
      console.error(err.stack);

      if (flight) {
        flight.stop();
      } else {
        process.exit(1);
      }
    });


  let triedExiting = false;

  process.on('SIGINT', () => {
    if (triedExiting) {
      process.exit(1);
      return;
    }

    triedExiting = true;

    if (flight) {
      flight.stop();
    } else {
      drone.disconnect()
        .then(() => {
          process.exit();
        })
        .catch((err) => {
          console.error(err.stack);
          process.exit(1);
        });
    }
  });
};
