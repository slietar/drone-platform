const wifiControl = require('./mz/wifi-control');

wifiControl.configure({ iface: 'en0' });


class WiFiController {
  constructor() {
    this.availableNetworks = null;
    this.currentNetwork = null;
    this.status = null;
  }

  connectToNetwork(network, { noNewAttempt } = {}) {
    return wifiControl.connectToAP({ ssid: network.ssid })
      .then(() => this.getCurrentNetwork({ force: true }))
      .catch((err) => {
        if (!noNewAttempt) {
          return this.connectToNetwork(network, { noNewAttempt: true });
        }

        throw err;
      });
  }

  ensureConnected() {
    return this.getCurrentNetwork({ force: true })
      .then((network) => {
        if (!this._isDroneNetwork(network)) {
          throw new Error('Not connected to a drone network');
        }
      });
  }

  findAvailableNetworks({ force, noNewAttempt } = {}) {
    if (!force && this.availableNetworks) {
      return Promise.resolve(this.availableNetworks);
    }

    return wifiControl.scanForWiFi()
      .then(({ networks }) => {
        if (!noNewAttempt && networks.length < 1) {
          return this.findAvailableNetworks({ force: true, noNewAttempt: true });
        }

        this.availableNetworks = networks;
        return this.findAvailableNetworks();
      });
  }

  findAvailableDroneNetworks({ force } = {}) {
    return this.findAvailableNetworks({ force })
      .then((networks) =>
        networks.filter((network) => this._isDroneNetwork(network)) // ardrone2_v2.4.8
      );
  }

  getCurrentNetwork({ force } = {}) {
    if (!force && this.currentNetwork) {
      return Promise.resolve(this.currentNetwork);
    }

    let wifiStatus = this.getWifiStatus({ force });

    return this.findAvailableNetworks()
      .then((networks) => {
        let matchingNetworks = networks.filter((network) => wifiStatus.ssid === network.ssid);

        if (matchingNetworks.length < 1) {
          throw new Error(`No network with SSID '${wifiStatus.ssid}'`);
        } else if (matchingNetworks.length > 1) {
          throw new Error(`More than one network with SSID '${wifiStatus.ssid}'`);
        }

        this.currentNetwork = matchingNetworks[0];

        return this.getCurrentNetwork();
      });
  }

  getWifiStatus({ force } = {}) {
    if (!force && this.status) {
      if (this.status.ssid === void 0) {
        throw new Error('Not connected');
      }

      return this.status;
    }

    this.status = wifiControl.getIfaceState();

    return this.getWifiStatus();
  }

  isConnectedToNetwork(network, { force } = {}) {
    return this.getCurrentNetwork({ force })
      .then((currentNetwork) => currentNetwork.mac === network.mac);
  }

  reset() {
    return wifiControl.resetWiFi();
  }

  _isDroneNetwork(network) {
    return /ardrone2_v\d+.\d+.\d+/.test(network.ssid);
  }
}


class DroneWiFiController {
  constructor() {
    this.controller = new WiFiController();
  }

  connect(behavior = 'auto') {
    if (behavior === 'fake') {
      let wifiStatus = this.controller.getWifiStatus();

      this.controller.currentNetwork = {
        mac: '00:00:00:00:00:00',
        signal_level: -30, // eslint-disable-line camelcase
        ssid: wifiStatus.ssid
      };

      return this.controller.getCurrentNetwork();
    }

    if (behavior === 'force-initial') {
      return this.controller.getCurrentNetwork();
    }

    if (behavior === 'initial') {
      return this.controller.findAvailableDroneNetworks()
        .then((droneNetworks) => {
          if (droneNetworks.length < 1) {
            return this.connect('force-initial');
          } if (droneNetworks.length > 1) {
            throw new Error('More than one drone network');
          }

          return this.connect('auto');
        });
    }

    if (behavior === 'auto') {
      let targetNetwork;

      return this.controller.findAvailableDroneNetworks()
        .then((droneNetworks) => {
          if (droneNetworks.length < 1) {
            throw new Error('No drone network');
          } if (droneNetworks.length > 1) {
            throw new Error('More than one drone network');
          }

          targetNetwork = droneNetworks[0];

          return this.controller.isConnectedToNetwork(targetNetwork);
        })
        .then((isConnected) => {
          if (isConnected) {
            return;
          }

          return this.controller.connectToNetwork(targetNetwork);
        })
        .then(() => this.controller.getCurrentNetwork());
    }

    let targetNetwork;

    return this.controller.findAvailableNetworks()
      .then((networks) => {
        targetNetwork = networks.find((network) => network.ssid === behavior);

        if (!targetNetwork) {
          throw new Error(`Network '${behavior}' does not exist`);
        }

        return this.controller.isConnectedToNetwork(targetNetwork);
      })
      .then((isConnected) => {
        if (isConnected) {
          return;
        }

        return this.controller.connectToNetwork(targetNetwork);
      })
      .then(() => this.controller.getCurrentNetwork());
  }

  disconnect() {
    return this.controller.reset();
  }
}


exports.WiFiController = WiFiController;
exports.DroneWiFiController = DroneWiFiController;
