exports.CONTROL_STATES = Object.freeze([
  'DEFAULT',
  'INIT',
  'LANDED',
  'FLYING',
  'HOVERING',
  'TEST',
  'TRANS_TAKEOFF',
  'TRANS_GOTOFIX',
  'TRANS_LANDING',
  'TRANS_LOOPING'
]);

exports.FLY_STATES = Object.freeze([
  'OK',
  'LOST_ALT',
  'LOST_ALT_GO_DOWN',
  'ALT_OUT_ZONE',
  'COMBINED_YAW',
  'BRAKE',
  'NO_VISION'
]);
