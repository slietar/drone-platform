const ardrone = require('ar-drone');
const { EventEmitter } = require('events');

const mzArdrone = require('../mz/ar-drone');
const DroneController = require('./controller');
const { DroneWiFiController } = require('../wifi-control');


class Drone extends EventEmitter {
  constructor(controlTower) {
    super();

    this.client = mzArdrone(ardrone.createClient());
    this.controller = new DroneController(this);
    this.controlTower = controlTower;
    this.wifiController = new DroneWiFiController();

    this.navData = {
      acceleration: { x: 0, y: 0, z: 0 },
      battery: 0,
      droneState: {},
      gps: { latitude: 0, longitude: 0, elevation: 0 },
      gyroscope: { x: 0, y: 0, z: 0 },
      magnetization: { x: 0, y: 0, z: 0 },
      pwm: { motors: [] },
      rotation: { x: 0, y: 0, z: 0 },
      satellites: [],
      timestamp: Date.now(),
      velocity: { x: 0, y: 0, z: 0 },
      toJSON() {
        return this;
      }
    };
  }

  connect(behavior) {
    return this.wifiController.connect(behavior);
  }

  disconnect() {
    return this.wifiController.disconnect();
  }

  initialize() {
    return this.setConfig()
      .then(() => this.waitForReady());
  }

  landInEmergency() {
    this.client.land();
    this.client.animateLeds('redSnake', 4, 10);
  }

  listenForNavData() {
    this.client.on('navdata', (data) => {
      if (!data.physMeasures || !data.magneto) {
        return;
      }

      let { x: ax, y: ay, z: az } = data.physMeasures.accelerometers;
      let { x: gx, y: gy, z: gz } = data.physMeasures.gyroscopes;
      let { x: mx, y: my, z: mz } = data.magneto.rectified;
      let { x: rx, y: ry, z: rz } = data.demo.rotation;
      let { x: vx, y: vy, z: vz } = data.demo.velocity;

      let navData = {};

      navData.acceleration = { x: ax, y: ay, z: az };
      navData.battery = data.demo.batteryPercentage;
      navData.gps = {
        latitude: data.gps.latFuse,
        longitude: data.gps.lonFuse,
        elevation: data.gps.elevation
      };

      navData.gyroscope = { x: gx, y: gy, z: gz };
      navData.magnetization = { x: mx, y: my, z: mz };
      navData.rotation = { x: rx, y: ry, z: rz };
      navData.velocity = { x: vx, y: vy, z: vz };

      navData.droneState = {
        // controlState: data.demo.controlState.substr(5), // remove 'CTRL_'
        // flyState: data.demo.flyState.substr(7), // remove 'FLYING_'

        gpsEphemerisStatus: Boolean(data.gps.ephermisStatus),
        gpsFirmwareStatus: Boolean(data.gps.firmwareStatus),
        gpsPlugged: Boolean(data.gps.gpsPlugged),
        wifiLinkQuality: Boolean(data.wifi.linkQuality)
      };

      navData.pwm = {
        motors: data.pwm.motors
      };

      for (let stateVarKey in data.droneState) {
        let newKey = stateVarKey.substr(0, 1).toLowerCase() + stateVarKey.substr(1);

        navData.droneState[newKey] = Boolean(data.droneState[stateVarKey]);
      }

      navData.gps.satellites = data.gps.channels
        .map((satellite) => ({ id: satellite.sat, signalStrength: satellite.cn0 }))
        .filter((satellite) => satellite.signalStrength > 0);

      navData.timestamp = Date.now();

      // makes 'toJSON()' non enumerable
      Reflect.defineProperty(navData, 'toJSON', {
        value() {
          return navData;
        }
      });

      this.navData = navData;
      this.emit('navData', this.navData);
    });
  }

  requestLanding() {
    return this.controlTower.requestLanding(this);
  }

  requestTakeOff() {
    return this.controlTower.requestTakeOff(this);
  }

  setConfig() {
    return Promise.all([
      this.client.config({ key: 'general:navdata_options', value: 777060865, timeout: 2000 }),
      this.client.config({ key: 'general:navdata_demo', value: 'FALSE', timeout: 2000 })
    ]);
  }

  waitForReady() {
    this.listenForNavData();

    return new Promise((resolve, reject) => {
      this.once('navData', resolve);
    });
  }
}


module.exports = Drone;
