const events = require('events');
const fs = require('fs');
const path = require('path');


class DroneController extends events.EventEmitter {
  constructor(drone) {
    super();

    this.drone = drone;
  }

  animateLeds(animation, frequency, duration) {
    this.emit('command', `Command: 'animateLeds'
  Animation name: ${animation}
  Frequency: ${frequency} Hz
  Duration: ${duration} sec`);

    this.client.animateLeds(animation, frequency, duration);

    return setPromiseTimeout(duration * 1e3);
  }

  calibrateMagnetometer() {
    this.emit('command', `Command: 'calibrate' device 0`);
    this.client.calibrate(0);
  }

  get client() {
    return this.drone.client;
  }

  land() {
    return this.drone.requestLanding()
      .then(() => {
        this.emit('command', `Command: 'land'`);
        return this.client.land();
      });
  }

  moveFront(speed, delay) {
    if (speed >= 0) {
      this.emit('command', `Command: 'front' at ${speed * 100}% of max speed`);
      this.client.front(speed);
    } else {
      this.emit('command', `Command: 'back' at ${-speed * 100}% of max speed`);
      this.client.back(-speed);
    }

    if (delay) {
      return setPromiseTimeout(delay)
        .then(() => {
          this.client.front(0);
          this.client.back(0);
        });
    }
  }

  moveLeft(speed, delay) {
    if (speed >= 0) {
      this.emit('command', `Command: 'left' at ${speed * 100}% of max speed`);
      this.client.left(speed);
    } else {
      this.emit('command', `Command: 'right' at ${-speed * 100}% of max speed`);
      this.client.right(-speed);
    }

    if (delay) {
      return setPromiseTimeout(delay)
        .then(() => {
          this.client.left(0);
          this.client.right(0);
        });
    }
  }

  moveUp(speed, delay) {
    if (speed >= 0) {
      this.emit('command', `Command: 'up' at ${speed * 100}% of max speed`);
      this.client.up(speed);
    } else {
      this.emit('command', `Command: 'down' at ${-speed * 100}% of max speed`);
      this.client.down(-speed);
    }

    if (delay) {
      return setPromiseTimeout(delay)
        .then(() => {
          this.client.up(0);
          this.client.down(0);
        });
    }
  }

  reset() {
    let deferred = Promise.defer();

    if (this.drone.navData.droneState.emergencyLanding) {
      this.emit('command', `Command: 'disableEmergency'`);
      this.client.disableEmergency();

      let listener = (navData) => {
        if (!navData.droneState.emergencyLanding) {
          deferred.resolve();
          this.drone.removeListener('navData', listener);
        }
      };

      this.drone.on('navData', listener);
    } else {
      deferred.resolve();
    }

    return deferred.promise;
  }

  rotate(speed) {
    if (speed >= 0) {
      this.emit('command', `Command: 'clockwise' at ${speed * 100}% of max speed`);
      this.client.clockwise(speed);
    } else {
      this.emit('command', `Command: 'counterClockwise' at ${-speed * 100}% of max speed`);
      this.client.counterClockwise(-speed);
    }
  }

  stop(delay) {
    this.emit('command', `Command: 'stop'`);
    this.client.stop();

    if (delay !== void 0) {
      return setPromiseTimeout(delay);
    }
  }

  takeOff() {
    return this.drone.requestTakeOff()
      .then(() => {
        this.emit('command', `Command: 'takeoff'`);
        return this.client.takeoff();
      });
  }

  takePhoto(filename) {
    this.emit('command', 'Taking photo');

    let imagePath = path.join(__dirname, '../../output/images', filename);

    let pngStream = this.client.getPngStream();

    pngStream.on('error', () => {

    });

    return new Promise((resolve) => {
      pngStream.once('data', (data) => {
        resolve(fs.writeFile(imagePath, data));
      });
    });
  }

  trimHorizontalRotation() {
    this.emit('command', `Command: 'ftrim'`);
    this.client.ftrim();
  }
}

function setPromiseTimeout(delay, value) {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(value), delay);
  });
}


module.exports = DroneController;
