let map = L.map('map').setView([45.216548, 5.808009], 19);

L.tileLayer('https://api.mapbox.com/styles/{id}/tiles/256/{z}/{x}/{y}?access_token={accessToken}', {
  attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
  maxZoom: 20,
  id: 'v1/mapbox/streets-v9',
  accessToken: 'pk.eyJ1Ijoic2ltOTc2MCIsImEiOiJ6c0pTSFAwIn0.hAe3Pa6uajoNoDLHh1WT5g'
}).addTo(map);

let realtime = L.realtime({
  url: 'http://localhost:3212',
  crossOrigin: true,
  type: 'json'
}, {
  interval: 0.5e3, // 0.1e3
  style: function (feature) {
    return feature.properties.style || {};
  }
}).addTo(map);


let latlngs = [
  [45.21647083016266, 5.80761378590742],
  [45.216506257660676, 5.807843951997735],
  [45.2165186333375, 5.808055884277005],
  [45.21643039363479, 5.808024058273959],
  [45.216626953123345, 5.8077487774888885],
  [45.21657007971234, 5.807656840967102]
];

L.polyline(latlngs).addTo(map);

for (let latlng of latlngs) {
  L.marker(latlng).addTo(map);
}

let circle = L.circle([0, 0], 3).addTo(map);

realtime.on('update', () => {
  /* let pos0 = realtime.getLayer('position').getLatLng();
  let pos1 = L.latLng(pos0.lat + 5e-5, pos0.lng);
  let radius = pos0.distanceTo(pos1); */

  circle.setLatLng(realtime.getLayer('position').getLatLng());
  // circle.setRadius(radius);
});

/* realtime.on('update', () => {
  realtime.getLayer('abc').setIcon(L.icon({
    iconUrl: 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/map-marker-icon.png',
    iconSize: [32, 32]
  }));
}); */
