# Things to do


* add a GPS data consistency check (e.g. consider 0.000042° as 0°) as a method in `Pilot`
* investigate and fix `navData.droneState.controlState` and `navData.droneState.flyState` issue
* improve WiFi control
* add a nav data reception frequency widget
* make charts depending on given timestamp and not real timestamp
* rewrite acceleration processor
* separate platform and pilot processes
* in `DroneController`, replace the string given in the `command` event with an object
* rewrite `RealtimeMapReporter`, using `socket.io` or websockets
* extend `AutoPilot` with a `GuardPilot` checking if the drone doesn't go out of a defined zone
* add an automatic magnetometer calibration before each flight
* check for problematic drone state properties (`lowBattery`, `motorProblem`, etc.) during flights
* check `gpsPlugged` and `gpsFirmwareStatus` drone state properties before/during flights
* smooth down `DroneController#moveFront()` and `DroneController#moveLeft()`
* add missing methods to `DroneController`
* update HTTP connection ports according to readme
* fix REPL problems
